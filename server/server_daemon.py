#!/usr/bin/env python

import sys, time, socket, datetime
from daemon import Daemon
from hooks import Hooks

sys.path.append('../utils')
from pyup_logger import Pyuplogger

class Pyupserver(Daemon):
    def run(self):
        logger = Pyuplogger()
        HOST = ''
        PORT = 5150
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((HOST, PORT))
        s.listen(1)
        
        logger.log('Server Started listening on: '+str(PORT))
        while 1:
            conn, addr = s.accept()
            print 'Connected by', addr
            while 1:
                data = conn.recv(1024)
                if not data: break
                try:
                    response = Hooks(data)
                    res = response.get_response()
                except Exception, e:
                    logger.log(timestamp+' '+str(e))
                try:
                    conn.sendall(res)
                except Exception, e:
                    logger.log(str(e))
                logger.log(' Response sent to client: '+res)
            conn.close()

if __name__ == "__main__":
    daemon = Pyupserver('/tmp/pyupserver.pid')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
