import json,sys
from uptime import uptime

sys.path.append('../utils')
from pyup_logger import Pyuplogger

class Hooks:
    def __init__(self, args):
        self.response = {}
        self.args = args.split(',')
        self.logger = Pyuplogger()
        self.methods = {'uptime':self.uptime}

    def get_response(self):
      for arg in self.args:
        if arg in self.methods:
          func = self.methods[arg]
          data = func.__func__()
          self.response[arg] = data
        else:
          self.logger.log('Invalid param')
      jsonstr = json.dumps(self.response, separators=(',',':'))
      return str(jsonstr)
	
    def uptime():
        up = uptime()
        return up.get_uptime()
