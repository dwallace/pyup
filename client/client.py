import socket
import sys

if len(sys.argv) > 1:
    HOST = sys.argv[1]
else:
    HOST = '127.0.0.1'
print "Connecting to "+HOST
PORT = 5150
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.connect((HOST, PORT))
except Exception, e:
    print "Connection could not be made."
    print >> sys.stderr, "Exception: %s" % str(e)
    sys.exit(1)
s.sendall('uptime')
data = s.recv(1024)
s.close()
print 'Received', repr(data)
