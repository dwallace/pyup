import time, datetime

class Pyuplogger:
    def log(self, str):
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        try:
            f = open('/var/tmp/pyup.log', 'a')
            f.write(timestamp+' '+str+'\n')
        except IOError:
            print 'Error writing to Log File'
        finally:
            f.close()